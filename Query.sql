CREATE DATABASE campeonatoM;
USE campeonatoM;
CREATE TABLE usuario(
	idUsuario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(30) NOT NULL,
	pais VARCHAR(50) NOT NULL,
	nick VARCHAR(20) NOT NULL,
	contrasena VARCHAR(20) NOT NULL,
	edad INT NOT NULL
);

CREATE TABLE encuentros(
	idEncuentros INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	idUserA INT NOT NULL,
	idUserB INT NOT NULL,
	FOREIGN KEY(idUserA) REFERENCES usuario(idUsuario),
	FOREIGN KEY(idUserB) REFERENCES usuario(idUsuario)
);

CREATE TABLE lineaEncuentros(
	idLinea INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	fecha DATETIME NOT NULL,
	descripcion TEXT,
	idEncuentro INT NOT NULL,
	FOREIGN KEY (idEncuentro) REFERENCES encuentros(idEncuentros)
); 

DELIMITER $$

CREATE TRIGGER tr_onAddEncuentro
AFTER INSERT encuentros
FOR EACH ROW 
BEGIN 
	DECLARE id_En INT;
	SET id_En = (SELECT idEncuentros FROM encuentros ORDER BY idEncuentros DESC LIMIT 1);
	INSERT INTO lineaEncuentros(fecha,descripcion, idEncuentro)
	VALUES (NOW(), "Se agrego un nuevo encuentro", id_En);
END $$


CREATE TRIGGER tr_onEditEncuentro
BEFORE UPDATE ON encuentros
FOR EACH ROW
BEGIN
	DECLARE id_En INT;
	INSERT INTO lineaEncuentros(fecha,descripcion, idEncuentro)
	VALUES (NOW(), "Se edito un encuentro", NEW.idEncuentro);
END$$

DELIMITER $$

CREATE TRIGGER tr_onDeleteEncuentro
AFTER DELETE ON encuentros
FOR EACH ROW
BEGIN
	INSERT INTO lineaEncuentros(fecha,descripcion, idEncuentro)
	VALUES (NOW(), "Se elimino un encuentro", OLD.idEncuentros);
END$$